<?php 
    if(!empty($_POST['Chat']['username'])&&!empty($_POST['Chat']['password'])){
?>       
<!DOCTYPE html> 
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Ask an Expert Chat History</title>
        <style>
            body{
                background-color: gray;
                padding:0px;
                margin:0px;
            }
            .container{
                background-color:white;
                width: 80%;
                margin-left: auto ;
                margin-right: auto ;
                min-height:800px;
                color:black;
            }
            .center-div{
                margin-left: auto ;
                margin-right: auto ;
                color:black;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="center-div" style="width: 90%; padding-top:20px;">Responses: <div style="float:right"><a href="index.php">Back</a></div></div>
        <div class="center-div"  style="width: 90%; min-height:800px; border:1px solid gray;">
        <textarea style="width: 100%; min-height:800px;" id="response_div" readonly=true></textarea>
             
        </div>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/chatjs/hmac-sha1.js"></script>
    <script src="js/chatjs/CryptoJS.js"></script>
    <script src="js/chatjs/beep.js"></script>
    <script src="js/chatjs/ContusFly.js"></script>
    <script src="js/chatjs/customchat.js"></script>
    <script>
        $(document).ready(function(){
    		if(shouldconnect()){
    			Fly.user='<?php echo $_POST['Chat']['username'];?>';
    			Fly.password='<?php echo $_POST['Chat']['password'];?>';
    		} else{
    			return false;
    		}
    	});
		var getUrl=function(param){
			return "";
		};
		var public_path = "";
		var chatImage="/img/chat-img.png";
		var chatExpert="Jaya Kumar";
    </script>
    	
    <script>
    function shouldconnect() {
	    var ua = window.navigator.userAgent;
	    var msie = ua.indexOf("MSIE ");

	    if (msie > 0) { // If Internet Explorer, return version number
	    	var ieversion = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
	    	if(ieversion<10){
	    		return 0;
	    	}
	    }
	    else{ // If another browser, return 0
	        return 1;
		}
	    return 1;
	}
	</script>
    </body>
</html>
<?php 
    } else{
        echo 'Username and Password should not be blank.';
    }
 ?>