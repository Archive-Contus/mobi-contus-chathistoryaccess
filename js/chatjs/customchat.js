var images={};
/*var chatWindows={};*/
function reopenChatWindow(obj) {
		var i=0;
		obj=jQuery.parseJSON(obj);
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				reopenChatPanel(obj[key]['user'],obj[key]['msg'],obj[key]['id'],obj[key]['from'],i++);
			}
		}
}
function reopenTempChatWindow(obj) {
	if (localStorage.opencount) {
		var i = Number(localStorage.opencount)+1;
	} else {
		var i = 1;
	}
	obj=jQuery.parseJSON(obj); 
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			DRchatConfirmation(obj[key]['user'],obj[key]['msg'],obj[key]['id'],obj[key]['from'],i++);
		}
	}
}
$(document).ready(function(){
	if(localStorage.getItem('chatWindows')){
		reopenChatWindow(localStorage.getItem('chatWindows'));
	}
	if(localStorage.getItem('tempChatWindows')){
		reopenTempChatWindow(localStorage.getItem('tempChatWindows'));
	}
	//Initialize the FLY Object
	Fly.init("askanexpert.expert","00f4675d2a936b2e6ad6f985d657bc09");
	//On Send Message Callbacks
	FlyCallback.onSend = function(id){
	$("#tick_"+id).append("<i class=\"icon icon_sent\"></i> ");
	};
	FlyCallback.onDelivered = function(id){
		$("#tick_"+id).append("<i class=\"icon icon_delivered\"></i>");
	};
	FlyCallback.onSeen = function(){
	};
	FlyCallback.onCommandReceived = function(from, type){
		var fromUser = from.split("@");
		var chatCode = generateChatCode(fromUser[0]);
		if(type=='sTartCHat'){
			chatConfirmation(fromUser[0],'',type,from);
		}else{
			if(type=="eNdChAt"){
			if(isChatWindowOpened(chatCode)){
				$("#"+chatCode).remove();
					obj=JSON.parse(localStorage.getItem('chatWindows'));
					delete obj[chatCode];
					localStorage.setItem('chatWindows',JSON.stringify(obj));
					images = JSON.parse(localStorage.getItem('chatImages'));
					delete images[chatCode];
					localStorage.setItem('chatImages',JSON.stringify(images));
					openchats = JSON.parse(localStorage.getItem('openChat'));
					openchats = removeElementByValue(openchats,chatCode);
					localStorage.setItem('openChat',JSON.stringify(openchats));
					localStorage.opencount = Number(localStorage.opencount)-1;
					rearrangeChatWindows();
					closeChatConfirmation(chatCode,fromUser[0],'','',from);
					if (localStorage.totOpencount) {
						localStorage.totOpencount = Number(localStorage.totOpencount)-1;
					}
			}
			return;
		} 
		}
	}
	FlyCallback.onMessageReceived = function(msg, from, id){
		var fromUser = from.split("@");
		var chatCode = generateChatCode(fromUser[0]);
		if(isChatWindowOpened(chatCode)){
			images = JSON.parse(localStorage.chatImages);
		   if(images[chatCode]['image']!=''&& images[chatCode]['image']!=null){
		   		var img_var = "<img src=\""+public_path+"/uploads/users/"+images[chatCode]['id']+"/"+images[chatCode]['image']+"\" class=\" img-responsive \">";
			} else{
				var img_var = "<img src=\""+public_path+"/images/chat-placholder.png\" class=\" img-responsive \">";
			}
			$("#pb_"+chatCode).append("<div class=\" msg_container base_receive\"><div class=\"col-md-2 col-xs-2 avatar_img pad0\"  style=\"min-width: 40px;\">"+img_var+"<span class=\"name\">"+images[chatCode]['name']+"</span></div><div class=\"avatar\"><div class=\"messages msg_receive\"><p>"+msg+"</p><time datetime=\"2009-11-13T20:00\"></time></div></div></div>");
			changeHeaderColor(chatCode, 1);
			scrollToBottom("pb_"+chatCode);
			
			Fly.sendReceipt(from, Fly.user+"@askanexpert.expert", id);
		}
		
	};
	$(document).on('keyup','textarea.send_message',function (event) {
		if (event.keyCode == 13) {                                                              
			var content = this.value;  
			var caret = getCaret(this); 
			var id = '#'+this.id;
			if(event.shiftKey){
				this.value = content.substring(0, caret - 1) + "\n" + content.substring(caret, content.length);
				event.stopPropagation();
			} else {
				var user = $(id).attr('rel');
				var chatCode = generateChatCode(user);
				sendMessage(chatCode, user);
			}
		}
	});
		
		});
		
		function sendMessage(id, user){
			var msg = $("#message_"+id).val(); //get message text
			if(msg==undefined || msg.length==1){
				$("#message_"+id).val('');
				return;
			}
			$("#message_"+id).val('');
			//Generating Message ID
			var msgId = id+"_"+getRandomInt(0, 1000000000000000);
			
			$("#pb_"+id).append("<div class=\"msg_container base_sent\"><div class=\"avatar\"><div class=\"messages msg_sent\"><p>"+msg+"</p><time datetime=\"2009-11-13T20:00\"></time><span class=\"tick\" id=\"tick_"+msgId+"\"></span></div></div><div class=\"col-md-2 col-xs-2 pad0 avatar_img\"><img src=\""+chatImage+"\" class=\"img-responsive \"><span class=\"name\">"+chatExpert+"</span></div></div>");
					
			var send = Fly.send(user+"@askanexpert.expert",msgId, msg);
			scrollToBottom("pb_"+id);
		}

		function flip(id){
			$("#"+id).slideToggle();
		}

		function getCaret(el) { 
			if (el.selectionStart) { 
				return el.selectionStart; 
			} else if (document.selection) { 
				el.focus();
				var r = document.selection.createRange(); 
				if (r == null) { 
					return 0;
				}
				var re = el.createTextRange(), rc = re.duplicate();
				re.moveToBookmark(r.getBookmark());
				rc.setEndPoint('EndToStart', re);
				return rc.text.length;
			}  
			return 0; 
		}
		
		function generateChatCode(usr){
			var str = Base64.encode(usr);
			str = str.replace(/=/g, "");
			return str;
		}
		
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
		
		var chatCodes = [];
		
		function openChat(user,msg,id,from){
			var chatWindows=JSON.parse(localStorage.getItem('chatWindows'));
			if(chatWindows==null){
				chatWindows={};
			}
			var chatCode = generateChatCode(user); 
			if(!isChatWindowOpened(chatCode)){
				var windowsOpen = getWindowOpenCount();
				var rightMargin = (290 * (windowsOpen - 1)) + 10;
				if(isLocalStorage()){ 
					var openchats = JSON.parse(localStorage.getItem('openChat'));
					if(openchats==null){
						openchats={};
					}
					openchats[chatCode] = chatCode; 
					localStorage.setItem('openChat',JSON.stringify(openchats));
					chatWindows[chatCode]={};
					chatWindows[chatCode]['id']=id;
					chatWindows[chatCode]['user']=user;
					chatWindows[chatCode]['msg']='';
					chatWindows[chatCode]['from']=from;
					localStorage.setItem('chatWindows',JSON.stringify(chatWindows));
				}
				getImage(chatCode,user,msg,rightMargin,id,from);
			}
			rearrangeChatWindows();
		}
		// reopen chat window on page refresh
		function reopenChatPanel(user,msg,id,from,i){
			var chatCode = generateChatCode(user);
			localImages=localStorage.getItem('chatImages');
			images = JSON.parse(localStorage.chatImages);
			if(isChatWindowOpened(chatCode)){
				var rightMargin = (290 * (i)) + 10;
				$(".container").append("<div class=\"chat-window col-xs-5 col-md-3\" onclick=\"changeHeaderColor('"+chatCode+"');\" id='"+chatCode+"' style=\"right:"+rightMargin+"px\"><div id='top_bar_"+chatCode+"' class=\"top-bar flip\" onclick=\"flip('pc_"+chatCode+"')\"><i class=\"icon icon_chat\"></i>"+images[chatCode]['name']+"<span class=\"badge pull-right red\">-</span></div><div class=\"panel-chat\"  id='pc_"+chatCode+"'><div class=\"panel-body msg_container_base\" id='pb_"+chatCode+"'></div><div class=\"panel-footer\"><textarea class=\"send_message\" id='message_"+chatCode+"' rel='"+user+"' type=\"text\" class=\"row col-lg-11 chat_input\" placeholder=\"Your Message ...\"></textarea><button class=\"icon icon_chat_send\" onclick=\"sendMessage('"+chatCode+"', '"+user+"')\" id=\"btn-chat\"></button></div></div></div>");
				$("#pc_"+chatCode).show();
				changeHeaderColor(chatCode, 1);
			}
		}
		function getImage(chatCode,user,msg,rightMargin,id,from){
			images[chatCode]={};
			images[chatCode]['id']='id';
			images[chatCode]['image']='';
			$.ajax({
				url:getUrl('getchatimage'),
				data:{user:user},
				success:function(response){
					images[chatCode]['id']=response[0].id;
					images[chatCode]['image']=response[0].imageUrl;
					images[chatCode]['name']=response[0].display_name;
					localStorage.setItem('chatImages',JSON.stringify(images));
					$(".container").append("<div class=\"chat-window col-xs-5 col-md-3\" onclick=\"changeHeaderColor('"+chatCode+"');\" id='"+chatCode+"' style=\"right:"+rightMargin+"px\"><div id='top_bar_"+chatCode+"' class=\"top-bar flip\" onclick=\"flip('pc_"+chatCode+"')\"><i class=\"icon icon_chat\"></i>"+images[chatCode]['name']+"<span class=\"badge pull-right red\">-</span></div><div class=\"panel-chat\" id='pc_"+chatCode+"'><div class=\"panel-body msg_container_base\" id='pb_"+chatCode+"'></div><div class=\"panel-footer\"><textarea class=\"send_message\" id='message_"+chatCode+"' rel='"+user+"' type=\"text\" class=\"row col-lg-11 chat_input\" placeholder=\"Your Message ...\"></textarea><button class=\"icon icon_chat_send\" onclick=\"sendMessage('"+chatCode+"', '"+user+"')\" id=\"btn-chat\"></button></div></div></div>");
					$("#pc_"+chatCode).show();
					if(msg!=''){
						if(images[chatCode]['image']!=''&& images[chatCode]['image']!=null){
							$("#pb_"+chatCode).append("<div class=\" msg_container base_receive\"><div class=\"col-md-2 col-xs-2 avatar_img pad0\" style=\"min-width: 40px;\"><img src=\""+public_path+"/uploads/users/"+images[chatCode]['id']+"/"+images[chatCode]['image']+"\" class=\" img-responsive \"> <span class=\"name\">"+images[chatCode]['name']+"</span></div><div class=\"avatar\"><div class=\"messages msg_receive\"><p>"+msg+"</p><time datetime=\"2009-11-13T20:00\"></time></div></div></div>");
						} else{
							$("#pb_"+chatCode).append("<div class=\" msg_container base_receive\"><div class=\"col-md-2 col-xs-2 avatar_img pad0\" style=\"min-width: 40px;\"><img src=\""+public_path+"/images/chat-placholder.png\" class=\" img-responsive \"> <span class=\"name\">"+images[chatCode]['name']+"</span></div><div class=\"avatar\"><div class=\"messages msg_receive\"><p>"+msg+"</p><time datetime=\"2009-11-13T20:00\"></time></div></div></div>");
						}
					}
					changeHeaderColor(chatCode, 1);
					scrollToBottom("pb_"+chatCode);
					Fly.sendReceipt(from, Fly.user+"@askanexpert.expert", id);
					
				}
			});
			return;
		}
		function changeHeaderColor(id, type){
			$("#pc_"+id).show();
			if(type == 1)
				$("#top_bar_"+id).css('background', '#f86e65');
			else
				$("#top_bar_"+id).css('background', '#006993');
		}
		
		function getWindowOpenCount(){
			if(isLocalStorage()) {
				 if (localStorage.opencount) {
					localStorage.opencount = Number(localStorage.opencount)+1;
				} else {
					localStorage.opencount = 1;
				}
				if (localStorage.totOpencount) {
					localStorage.totOpencount = Number(localStorage.totOpencount)+1;
				}
				return localStorage.opencount;
			}
		}
		function resetWindowOpenCount(){
			if(isLocalStorage()) {
				localStorage.opencount = 0;
				localStorage.openChat = 0;
			}
		}
		
		function isLocalStorage(){
			if(typeof(Storage) !== "undefined") return true; else return false;
		}
		
		function isChatWindowOpened(chatCode){
			if(isLocalStorage() && localStorage.openChat){
				var obj=localStorage.openChat;
				obj=jQuery.parseJSON(obj);
				for (var key in obj) {
					if (obj.hasOwnProperty(key)) {
						if(key==chatCode){
							return true;
						}
					}
				}
			}
			return false;
		}
		function isTempChatWindowOpened(chatCode){
			var obj = localStorage.getItem('tempChatWindows');
			obj=jQuery.parseJSON(obj);
			for (var key in obj) {
					if(key==chatCode){ 
						return true;
					}
			}
			var openchatobj=localStorage.getItem('openChat');
			openchatobj=jQuery.parseJSON(openchatobj);
			for (var key in openchatobj) {
				if (openchatobj.hasOwnProperty(key)) {
					if(key==chatCode){
						return true;
					}
				}
			}
			return false;
		}
		function scrollToBottom(id){
			var objDiv = document.getElementById(id);
			objDiv.scrollTop = objDiv.scrollHeight;
		}
		
		function sleep(milliseconds) {
		  var start = new Date().getTime();
		  for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
			  break;
			}
		  }
		}
		function removeElementByValue(arr,value){
			for (var key in arr) {
				if (arr.hasOwnProperty(key)) {
					if(arr[key]==value){
						delete arr[key];
					}
				}
			}
			return arr;
		}
		function rearrangeChatWindows(){
			var openchats = JSON.parse(localStorage.getItem('openChat'));
			var c;
			var i=0;
			for (c in openchats){
				if (openchats.hasOwnProperty(c)) {
					if(openchats[c]!=null){
						var rightMargin = (290 * (i)) + 10;
						$('#'+openchats[c]).css("right",rightMargin);
						i++;
					}
				}
			}
			var opentempchats = JSON.parse(localStorage.getItem('tempChatWindows'));
			for (c in opentempchats){
				if (opentempchats.hasOwnProperty(c)) {
					if(opentempchats[c]!=null){
						var tmprightMargin = (290 * (i)) + 10;
						$('#chat_overlay_'+c).css("right",tmprightMargin);
						i++;
					}
				}
			}
			
			
		}
		function chatConfirmation(user,msg,id,from){ 
			var chatCode = generateChatCode(user);
			var windowsOpen = 1;
			if(!isTempChatWindowOpened(chatCode)){
				windowsOpen = getTempChatWindowOpened();
				var rightMargin = (290 * (windowsOpen-1)) + 10;
				$.ajax({
					url:getUrl('getchatimage'),
					data:{user:user},
					success:function(response){
					    if(response[0].imageUrl!=''&& response[0].imageUrl!=null){
					   		var img_var = "<img src=\""+public_path+"/uploads/users/"+response[0].id+"/"+response[0].imageUrl+"\" class=\" img-responsive \">";
						} else{
							var img_var = "<img src=\""+public_path+"/images/chat-placholder.png\" class=\" img-responsive \">";
						}
						$(".container").append(
								"<div class=\"chat-window col-xs-5 col-md-3\" id='chat_overlay_"+chatCode+"' style=\"right:"+rightMargin+"px\">" +
								"<div id='top_bar_"+chatCode+"' class=\"top-bar flip\" >" +
								"<i class=\"icon icon_chat\"></i>"+response[0].display_name+
								"</div>" +
								"<div class=\"msg_container_base\" style=\"min-height:350px\">" +
								"<div class=\"msg_receive\" style=\"padding: 10px 20px;\">" +
								"<div class=\"col-md-2 col-xs-2 avatar_img pad0\"  style=\"min-width: 40px;\">"+img_var+"<span class=\"name\"></span></div>" +
								"" +
								"<p style=\"clear:both;\"> Do you accept this chat ? </p><button type=\"button\" class=\"btn-group btn-sm btn-success\" style=\"margin-right:5px\" onclick=\"accept_chat('"+chatCode+"','"+user+"','','"+id+"','"+from+"');\">Accept</button><button type=\"button\" class=\"btn-group btn-sm btn-danger\"  onclick=\"deny_chat('"+chatCode+"','"+user+"','','"+id+"','"+from+"');\">Deny</button></div></div>"+
								"</div>");
						}
				});
				
				if(!checkWindowExist(chatCode)){
					var TempchatWindows = JSON.parse(localStorage.getItem('tempChatWindows'));
					if(TempchatWindows==null){
						var TempchatWindows={};
					}
					TempchatWindows[chatCode]={};
					TempchatWindows[chatCode]['id']=id;
					TempchatWindows[chatCode]['user']=user;
					TempchatWindows[chatCode]['msg']='';
					TempchatWindows[chatCode]['from']=from;
					localStorage.setItem('tempChatWindows',JSON.stringify(TempchatWindows));
					if (localStorage.totOpencount) {
						localStorage.totOpencount = Number(localStorage.totOpencount)+1;
					}else{
						localStorage.totOpencount = 1;
					}
				}
			}
		}
		function DRchatConfirmation(user,msg,id,from,i){ 
			var chatCode = generateChatCode(user);
			var windowsOpen = 1;
				windowsOpen=i;
				var rightMargin = (290 * (windowsOpen-1)) + 10;
				$.ajax({
					url:getUrl('getchatimage'),
					data:{user:user},
					success:function(response){
					    if(response[0].imageUrl!=''&& response[0].imageUrl!=null){
					   		var img_var = "<img src=\""+public_path+"/uploads/users/"+response[0].id+"/"+response[0].imageUrl+"\" class=\" img-responsive \">";
						} else{
							var img_var = "<img src=\""+public_path+"/images/chat-placholder.png\" class=\" img-responsive \">";
						}
						$(".container").append(
								"<div class=\"chat-window col-xs-5 col-md-3\" id='chat_overlay_"+chatCode+"' style=\"right:"+rightMargin+"px\">" +
								"<div id='top_bar_"+chatCode+"' class=\"top-bar flip\" >" +
								"<i class=\"icon icon_chat\"></i>"+response[0].display_name+
								"</div>" +
								"<div class=\"msg_container_base\" style=\"min-height:350px\">" +
								"<div class=\"msg_receive\" style=\"padding: 10px 20px;\">" +
								"<div class=\"col-md-2 col-xs-2 avatar_img pad0\"  style=\"min-width: 40px;\">"+img_var+"<span class=\"name\"></span></div>" +
								"" +
								"<p style=\"clear:both;\"> Do you accept this chat ? </p><button type=\"button\" class=\"btn-group btn-sm btn-success\" style=\"margin-right:5px\" onclick=\"accept_chat('"+chatCode+"','"+user+"','','"+id+"','"+from+"');\">Accept</button><button type=\"button\" class=\"btn-group btn-sm btn-danger\"  onclick=\"deny_chat('"+chatCode+"','"+user+"','','"+id+"','"+from+"');\">Deny</button></div></div>"+
								"</div>");
						}
				});
		}
		function accept_chat(chatCode,user,msg,id,from){
			$('#chat_overlay_'+chatCode).remove();
			var msgId = id+"_"+getRandomInt(0, 1000000000000000);
			Fly.sendCommand(user+"@askanexpert.expert",from, 1);
			var tempchatWindows = JSON.parse(localStorage.getItem('tempChatWindows'));
			delete tempchatWindows[chatCode];
			localStorage.setItem('tempChatWindows',JSON.stringify(tempchatWindows));
			if (localStorage.totOpencount) {
				localStorage.totOpencount = Number(localStorage.totOpencount)-1;
			}
			openChat(user,msg,id,from);
		}
		function deny_chat(chatCode,user,msg,id,from){
			$('#chat_overlay_'+chatCode).remove();
			var msgId = id+"_"+getRandomInt(0, 1000000000000000);
			Fly.sendCommand(user+"@askanexpert.expert",from, 0);
			var tempchatWindows = JSON.parse(localStorage.getItem('tempChatWindows'));
			delete tempchatWindows[chatCode];
			localStorage.setItem('tempChatWindows',JSON.stringify(tempchatWindows));
			if (localStorage.totOpencount) {
				localStorage.totOpencount = Number(localStorage.totOpencount)-1;
			}
		}
		function closeChatConfirmation(chatCode,user,msg,id,from){
			var tempchatWindows = JSON.parse(localStorage.getItem('tempChatWindows'));
			if(tempchatWindows[chatCode]){
				delete tempchatWindows[chatCode];
				localStorage.setItem('tempChatWindows',JSON.stringify(tempchatWindows));
				if (localStorage.totOpencount) {
					localStorage.totOpencount = Number(localStorage.totOpencount)-1;
				}
			}
		}
		function getTempChatWindowOpened(){
			var windowsOpen=0;
			if (localStorage.totOpencount) {
				windowsOpen = Number(localStorage.totOpencount)+1;
			}
			if(windowsOpen<1){
				windowsOpen=1;
			}
			return windowsOpen;
		}
		function checkWindowExist(chatCode){
			var pchat = localStorage.getItem('chatWindows');
			var pchatWindows=JSON.parse(pchat);
			var tchat = localStorage.getItem('tempChatWindows');
			var tchatWindows=JSON.parse(tchat);
			var popen= false;
			var topen= false;
			if(isLocalStorage() && pchatWindows ){
				popen = $.inArray(chatCode, pchatWindows) > -1;
			}
			if(isLocalStorage() && tchatWindows){
				topen = $.inArray(chatCode, tchatWindows) > -1;
			}
			if(popen || topen){
				return true;
			} else{
				return false;
			}
			return false;
		}