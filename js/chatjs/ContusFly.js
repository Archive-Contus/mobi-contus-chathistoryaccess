/*
 * Contus Fly Library
 * This is a library to connect the Chat API from the Web Chat Client. 
 * version: 1.0   
 * Author & support : <Sathish Kumar> sathishkumar@contus.in 
 * © 2015 contus.com
 */
function shouldconnect() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) { // If Internet Explorer, return version number
    	var ieversion = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    	if(ieversion<10){
    		return 0;
    	}
    }
    else{ // If another browser, return 0
        return 1;
	}
    return 1;
}

var Fly = Fly || (function(){
	if(shouldconnect()){
	} else{
		return false;
	}
	var domain ="";
	var key = "";
	var chatWith = "";
	var isConnected;
	//WebSocket variables
	var ws;
	var wsURI;
	var rescount = 1;
	
	return {
		init:function(domain, key) {
			this.domain = domain;
			this.key = key;
			wsURI = "wss://"+this.domain+":5223/xmpp";
			//create a new WebSocket object.	
			 ws = new WebSocket(wsURI, ['xmpp']);
			 ws.onopen = function(evt) { onOpen(evt) }; 
			 ws.onclose = function(evt) { onClose(evt) }; 
			 ws.onmessage = function(evt) { onMessage(evt) }; 
			 ws.onerror = function(evt) { onError(evt) };
		},
		connect: function(){
		},
		send: function(to, id, text){
			sendMessage(to, id, text);
		},
		sendReceipt: function(to, from, id){
			ws.send("<message from=\""+from+"\" to=\""+to+"\" type=\"receipts\" id=\""+id+"\" xmlns=\"jabber:client\"><rec:request xmlns:rec=\"urn:xmpp:receipts\"/></message>");
		},
		sendCommand: function(to, from, status){
			ws.send("<message to='"+to+"' type='command' message_type='text' id='"+status+"' xmlns:cli='jabber:client'><body>"+status+"</body><request xmlns:rec='urn:xmpp:receipts'/></message>");
		},
		user:'',
		password:'',
		from:''
	}

	function onOpen(evt) { 
		console.log("Connected"); 
		login(Fly.user, Fly.password); 
		console.log("Logging in...");
		chatHistory();
		/*getProfileDetails(); */
	}  
	function onClose(evt) { console.log("Disconnected"); isConnected = false;  login(Fly.user, Fly.password);console.log("Logging in...");}  
	function onMessage(evt) { processResponse(evt.data); }  
	function onError(evt) { isConnected = false;}
	
	function chatHistory(){ alert("chat histories got");
		ws.send('<iq type="get"><query xmlns="jabber:iq:chat_log"><chat_log user="'+Fly.user+'" chatsessionid="121212" limit="20" status="get_recent"/></query></iq>');
	}
	function login(user, pass){
		$('#message_box').html("<div class=\"system_msg\">Logging in...</div>");
		var base =  Base64.encode(String.fromCharCode(0)+user+String.fromCharCode(0)+pass);
		ws.send("<start><auth xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\" mechanism=\"PLAIN\" to=\"askanexpert.expert\">"+base+"</auth>");
	}

	function authenticate(sessionId){
		//var hash = Base64.encode(CryptoJS.HmacSHA1(sessionId, Fly.key).toString());
		var hash1 = CryptoJS.HmacSHA1(sessionId, Fly.key).toString(CryptoJS.enc.Base64);
		ws.send("<iq type=\"set\" id=\"1\"><bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\"><hash>"+hash1+"</hash><resource>WebSocketClient</resource></bind></iq>");
	}

	function processResponse(string){ 
		
		
		var xml = parseXML(string);
		var hrul = "-----------------------------------------Response#"+(rescount++)+"-------------------------------------------------------------\n";
		var content = $("#response_div").html();
		$("#response_div").html(content+hrul+string+'\n\n\n');
		//Log-in session
		var session = xml.getElementsByTagName("session");
		if(session.length > 0){
			if(string.indexOf("sessid") > -1){
			var sessionId = validateParseXML(session, "sessid");
			//Authenticate the Session ID.
			$('#connection').html("Authentication in progress...");
			authenticate(sessionId);
			}
		}
		//Authentication success
		if(string.indexOf("jid") > -1 && string.indexOf("test1@"+domain) > -1){
			//Login and Authentication Success
			isConnected = true;
			$('#connection').html("Online");
			getContacts();
			//getProfileDetails();
		}
		
		
		//Check a Ping from server
		if(string.indexOf("urn:xmpp:ping") > -1 && string.indexOf("id") > -1){
			var iq = xml.getElementsByTagName("iq");
			var id = validateParseXML(iq, "id");
			//Ping Server to inform available
			pingServer(id);
		}
		
		//Get Contacts
		if(string.indexOf("get_contacts") > -1 && string.indexOf("get_contacts") > -1){
			var contacts = xml.getElementsByTagName("contact");
			displayContacts(contacts);
		}
		
		//Profile Details
		if(string.indexOf("jabber:iq:profile") > -1){
			//processReceivedMessage(xml);
		}
		
		//Receive Message
		if(string.indexOf("message") > -1){  
			processReceivedMessage(xml);
		}
		//Receive Message
		if(string.indexOf("type='command'") > -1){
			processReceivedMessage(xml);
		}
	}

	function pingServer(id){
		ws.send("<iq to='askanexpert.expert' id='"+id+"' type='result'><ping xmlns='urn:xmpp:ping'/></iq>");
	}

	function getContacts(){
		ws.send("<iq type='get'><query xmlns='jabber:iq:contact'><contact status='get_contacts'/></query></iq>");
	}
	
	function getProfileDetails(){ alert("Profile Details Got");
		ws.send('<iq type="get"><query xmlns="jabber:iq:profile"><profile user="'+Fly.user+'" status="get_profile"/></query></iq>');
	}

	function displayContacts(obj){
		var length = obj.length;
		if(length > 0){
			var value = "";
			for(var i = 0; i < length; i++) {
				xmlAttributes = obj[i].attributes;
						
				value +="<li class='base_receive' onclick=openChat('"+xmlAttributes.getNamedItem("user").nodeValue+"')><div class=\"avatar avatar_img\" style=\"float: left;\"><img class='img-responsive' src=\""+xmlAttributes.getNamedItem("image").nodeValue+"\"/></div><div>"+xmlAttributes.getNamedItem("user").nodeValue+"</div></li>"
			}
			$('#contact-list').html(value);
		}
	}

	function processReceivedMessage(xml){
		var message = xml.getElementsByTagName("message");
		var length = message.length;
		if(length > 0){
			var msg = "";
			var id ="";
			for(var i = 0; i < length; i++) {
				xmlAttributes = message[i].attributes;
				id = xmlAttributes.getNamedItem("id").nodeValue;
				
				//Message Received from XYZ
				if(xmlAttributes.getNamedItem("from").nodeValue.indexOf(Fly.domain) > -1 && xml.getElementsByTagName("body").length !=0 && xmlAttributes.getNamedItem("type").nodeValue == "chat"){
					msg = xml.getElementsByTagName("body");
				}
				//Send Message had been Sent to XYZ
				if(xmlAttributes.getNamedItem("from").nodeValue.indexOf(Fly.domain) > -1 && xmlAttributes.getNamedItem("type").nodeValue == "receipt"){
					FlyCallback.onSend(id);
				}
				//Send Message had been Delivered to XYZ
				if(xmlAttributes.getNamedItem("from").nodeValue.indexOf(Fly.domain) > -1 && xmlAttributes.getNamedItem("type").nodeValue == "receipts"){
					FlyCallback.onDelivered(id);
				}
				if(xmlAttributes.getNamedItem("from").nodeValue.indexOf(Fly.domain) > -1 && xmlAttributes.getNamedItem("type").nodeValue == "command"){
					FlyCallback.onCommandReceived(xmlAttributes.getNamedItem("from").nodeValue,id);
				}
			}
			if(msg != ""){
				FlyCallback.onMessageReceived(decodeMessage(msg[0].innerHTML), xmlAttributes.getNamedItem("from").nodeValue, id);
				jBeep();
			}
		}
	}

	function decodeMessage(msg){
		return decodeURIComponent(msg).replace(/\++/g, ' ');
	}

	function validateParseXML(obj, tag){
		var length = obj.length;
		if(length > 0){ // Session Success
			var value;
			for(var i = 0; i < length; i++) {
				xmlAttributes = obj[i].attributes;
				value = xmlAttributes.getNamedItem(tag).nodeValue;
			}
			return value;
		}
	}

	//DOM Parse XML
	function parseXML(text) {
		if (window.DOMParser) {
			parser = new DOMParser();
			doc = parser.parseFromString(text,"text/html");
		}
		else { // Internet Explorer
			doc = new ActiveXObject("Microsoft.XMLDOM");
			doc.async="false";
			doc.loadXML(text);
		}
		return doc;
	}

	function sendMessage(to, id, msg){
		chatWith = to;
		ws.send("<message to='"+chatWith+"' type='chat' message_type='text' id='"+id+"' xmlns:cli='jabber:client'><body>"+msg+"</body><request xmlns:rec='urn:xmpp:receipts'/></message>");
		//FlyCallback.onSend();
	}
	
		
}());



var FlyCallback = function(){}

FlyCallback.prototype.onSend = function(id){
}

FlyCallback.prototype.onDelivered = function(id){
	
};

FlyCallback.prototype.onSeen = function(id){
	
};

FlyCallback.prototype.onMessageReceived = function(msg, from){
	
};

/**
	* Base64 Encode and Decode
	*/
	var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },


    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }
}


/**
 * jBeep
 * 
 * Play WAV beeps easily in javascript!
 * Tested on all popular browsers and works perfectly, including IE6.
 * 
 * @date 10-19-2012
 * @license MIT
 * @author Everton (www.ultraduz.com.br)
 * @version 1.0
 * @params soundFile The .WAV sound path
 */
function jBeep(soundFile){
	
	if (!soundFile) soundFile = public_path+"/js/chatjs/sounds/message.mp3";
	
	var soundElem, bodyElem, isHTML5;
	
	isHTML5 = true;
	try {
		if (typeof document.createElement("audio").play=="undefined") isHTML5 = false;
	}
	catch (ex){
		isHTML5 = false;
	}	

	bodyElem = document.getElementsByTagName("body")[0];	
	if (!bodyElem) bodyElem = document.getElementsByTagName("html")[0];
	
	soundElem = document.getElementById("jBeep");		
	if (soundElem) bodyElem.removeChild(soundElem);

	if (isHTML5) {

		soundElem = document.createElement("audio");
		soundElem.setAttribute("id", "jBeep");
		soundElem.setAttribute("src", soundFile);
		soundElem.play();

	}
	else if(navigator.userAgent.toLowerCase().indexOf("msie")>-1){		
		
		soundElem = document.createElement("bgsound");
		soundElem.setAttribute("id", "jBeep");
		soundElem.setAttribute("loop", 1);
		soundElem.setAttribute("src", soundFile);

		bodyElem.appendChild(soundElem);

	}
	else {
		
		var paramElem;
		
		soundElem = document.createElement("object");
		soundElem.setAttribute("id", "jBeep");
		soundElem.setAttribute("type", "audio/wav");
		soundElem.setAttribute("style", "display:none;");
		soundElem.setAttribute("data", soundFile);
		
		paramElem = document.createElement("param");
		paramElem.setAttribute("name", "autostart");
		paramElem.setAttribute("value", "false");
		
		soundElem.appendChild(paramElem);
		bodyElem.appendChild(soundElem);
		
		try {
			soundElem.Play();
		}
		catch (ex) {
			soundElem.object.Play();
		}
		
	}
	
}

	
