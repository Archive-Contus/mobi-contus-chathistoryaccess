<!DOCTYPE html> 
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Ask an Expert Chat History</title>
        <style>
            body{
                background-color: gray;
                padding:0px;
                margin:0px;
            }
            .container{
                background-color:white;
                width: 80%;
                margin-left: auto ;
                margin-right: auto ;
                min-height:800px;
            }
            .center-div{
                margin-left: auto ;
                margin-right: auto ;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="center-div" style="width: 300px; padding:50px;">
            <form method="post" id="chat_form" action="response.php">
                Username : <input type="text" name="Chat[username]"></br>
                Password : <input type="password" name="Chat[password]"></br>
                <input type="submit" value="Submit">
            </form>
        </div>
    </div>
    </body>
</html>